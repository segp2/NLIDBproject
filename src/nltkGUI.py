import os
import sys
import nlpcode2
from gi import require_version as gi_require_version
gi_require_version('Gtk', '3.0')
gi_require_version('GtkSource', '3.0')
from gi.repository import Gtk
import time
import locale

import gtk

# from gtkspellcheck import SpellChecker


# from pandas import *

NAME = 'GtkApp'
__version__ = '0.0.1'
VERSIONSTR = '{} v. {}'.format(NAME, __version__)


class App(Gtk.Window):
    store = []
    pro = nlpcode2.nlp()
    """ Main window with all components. """
    def __init__(self):
        Gtk.Window.__init__(self)
        nlpcode2.nlp.__init__(self.pro)
        self.builder = Gtk.Builder()
        gladefile = '/home/casper/PycharmProjects/start/try.glade'
        if not os.path.exists(gladefile):
            # Look for glade file in this project's directory.
            gladefile = os.path.join(sys.path[0], gladefile)

        try:
            self.builder.add_from_file(gladefile)
        except Exception as ex:
            print('\nError building main window!\n{}'.format(ex))
            sys.exit(1)

        # Get gui objects
        self.adjustment1 = self.builder.get_object('adjustment1')
        self.adjustment2 = self.builder.get_object('adjustment2')
        self.adjustment3 = self.builder.get_object('adjustment3')
        self.adjustment4 = self.builder.get_object('adjustment4')
        self.box10 = self.builder.get_object('box10')
        self.box2 = self.builder.get_object('box2')
        self.box3 = self.builder.get_object('box3')
        self.box4 = self.builder.get_object('box4')
        self.box5 = self.builder.get_object('box5')
        self.box6 = self.builder.get_object('box6')
        self.box7 = self.builder.get_object('box7')
        self.box8 = self.builder.get_object('box8')
        self.box9 = self.builder.get_object('box9')
        self.button1 = self.builder.get_object('button1')
        self.button2 = self.builder.get_object('button2')
        self.button3 = self.builder.get_object('button3')
        self.button4 = self.builder.get_object('button4')
        self.button5 = self.builder.get_object('button5')
        self.button6 = self.builder.get_object('button6')
        self.button7 = self.builder.get_object('button7')
        self.cellrenderertext1 = self.builder.get_object('cellrenderertext1')
        self.cellrenderertext2 = self.builder.get_object('cellrenderertext2')
        self.cellrenderertext3 = self.builder.get_object('cellrenderertext3')
        self.cellrenderertext4 = self.builder.get_object('cellrenderertext4')
        self.cellrenderertext5 = self.builder.get_object('cellrenderertext5')
        self.entry1 = self.builder.get_object('entry1')
        self.entry2 = self.builder.get_object('entry2')
        self.entrybuffer1 = self.builder.get_object('entrybuffer1')
        self.entrycompletion1 = self.builder.get_object('entrycompletion1')
        self.filechooserwidget1 = self.builder.get_object('filechooserwidget1')
        self.label1 = self.builder.get_object('label1')
        self.label2 = self.builder.get_object('label2')
        self.label3 = self.builder.get_object('label3')
        self.label4 = self.builder.get_object('label4')
        self.label5 = self.builder.get_object('label5')
        self.label6 = self.builder.get_object('label6')
        self.liststore2 = self.builder.get_object('liststore2')
        self.liststore3 = self.builder.get_object('liststore3')
        self.messagedialog1 = self.builder.get_object('messagedialog1')
        self.messagedialogaction_area = self.builder.get_object('messagedialogaction_area')
        self.messagedialogvbox = self.builder.get_object('messagedialogvbox')
        self.notebook1 = self.builder.get_object('notebook1')
        self.progressbar1 = self.builder.get_object('progressbar1')
        self.scrolledwindow1 = self.builder.get_object('scrolledwindow1')
        self.scrolledwindow2 = self.builder.get_object('scrolledwindow2')
        self.textbuffer1 = self.builder.get_object('textbuffer1')
        self.textview1 = self.builder.get_object('textview1')
        self.treeviewselection = self.builder.get_object('treeviewselection')
        self.treeview1 = self.builder.get_object('treeview1')
        self.treeview2 = self.builder.get_object('treeview2')
        self.treeviewcolumn1 = self.builder.get_object('treeviewcolumn1')
        self.treeviewcolumn2 = self.builder.get_object('treeviewcolumn2')
        self.treeviewcolumn3 = self.builder.get_object('treeviewcolumn3')
        self.treeviewcolumn4 = self.builder.get_object('treeviewcolumn4')
        self.treeviewcolumn5 = self.builder.get_object('treeviewcolumn5')
        self.treeviewselection2 = self.builder.get_object('treeviewselection2')
        self.window1 = self.builder.get_object('window1')


            # Connect all signals.
        self.builder.connect_signals(self)

        # self.liststore2.append(["b", "Row 1"])
        # self.liststore2.append(["c", "Row 2"])
        # #
        # self.liststore3.append(["b", "Row 1", "Row 1"])
        # self.liststore3.append(["c", "Row 2", "Row 1"])
        # self.liststore3.append(["d", "Row 3", "Row 1"])
        # self.liststore3.append(["e", "Row 4", "Row 1"])
        # self.liststore3.append(["f", "Row 5", "Row 1"])
        # self.liststore3.append(["g", "Row 4", "Row 1"])
        # self.liststore3.append(["h", "Row 5", "Row 1"])

        # self.cellrenderertext1.set_property("editable", True)
        self.cellrenderertext2.set_property("editable", True)

        # self.cellrenderertext3.set_property("editable", True)
        self.cellrenderertext4.set_property("editable", True)
        # self.cellrenderertext5.set_property("editable", True)

        self.treeviewcolumn1.add_attribute(self.cellrenderertext1, "text", 0)
        self.treeviewcolumn2.add_attribute(self.cellrenderertext2, "text", 1)

        self.treeviewcolumn3.add_attribute(self.cellrenderertext3, "text", 0)
        self.treeviewcolumn4.add_attribute(self.cellrenderertext4, "text", 1)
        self.treeviewcolumn5.add_attribute(self.cellrenderertext5, "text", 2)




        # Show the main window.
        self.window1.show_all()


    #integrationn code
    def populate_tree(self):
        # self.liststore2.append(["b", "Row 1,Row,Three"])

        print("I am working")
        database = self.pro.db
        # print(database)
        for key in database:
            self.liststore2.append([key, ""])
            # self.liststore3.append([key,"",""])
            for val in database[key]:
                self.liststore2.append([val,""])
                # self.liststore3.append([val,"",""])

        # print(database)


        for key1 in database:
            for key in database:
                path=self.pro.find_path(key1,key)
                print(path)
                if len(path)>1:
                    self.liststore3.append([key1, " ", key])

                # self.liststore3.append([val,"",""])
        pass

    def on_btnYes_clicked(self, widget, user_data=None):
        """ Handler for on.clicked. """
        str =  self.filechooserwidget1.get_preview_uri()
        print (str)
        response = self.messagedialog1.run()
        pass



    def item_selected(self,selection):
        model, row = selection.get_selected()
        if row is not None:
            print ("name", model[row][1])
            print("age" , model[row][1] )

    def _text_changed(self, w, row, new_value, column):
        print(row)
        print(column)
        self.liststores[row][column] = new_value
        print(row)
        print(column)


    def on_btnFind_clicked(self, widget, user_data=None):
        """ Handler for on.clicked. """
        # pro = nlpcode.nlp
        text= self.entry2.get_text()
        print("Got the input text")
        Squery = self.pro.main(text,self.progressbar1)
        print(Squery)
        # time.sleep(5)
        # self.progressbar1.set_fraction(0.5)

        self.textbuffer1.set_text(Squery)

        pass

    def on_btnDown_clicked(self, widget, user_data=None):
        """ Handler for on.clicked. """
        print ("pyGUI Clicked")
        # get the current page number, page numbers start at 0
        self.current_page = self.notebook1.next_page()
        pass

    def on_cellrenderertext1_edited(self, widget, path, new_text, user_data=None):
        """ Handler for on.edited. """
        print("on_cellrenderertext1_edited")
        print(path)
        print(new_text)
        self.liststore2[path][0] = new_text
        pass


    def on_cellrenderertext2_edited(self, widget, path, new_text, user_data=None):
        """ Handler for on.edited. """
        print("on_cellrenderertext2_edited")
        print(path)
        print(new_text)
        self.liststore2[path][1] = new_text
        pass

    def on_cellrenderertext3_edited(self, widget, path, new_text, user_data=None):
        """ Handler for on.edited. """
        print("on_cellrenderertext3_edited")
        print(path)
        print(new_text)
        self.liststore3[path][2] = new_text
        pass

    def on_cellrenderertext4_edited(self, widget, path, new_text, user_data=None):
        """ Handler for on.edited. """
        print("on_cellrenderertext4_edited")
        print(path)
        print(new_text)
        self.liststore3[path][1] = new_text
        pass

    def on_cellrenderertext5_edited(self, widget, path, new_text, user_data=None):
        """ Handler for on.edited. """
        print("on_cellrenderertext5_edited")
        print(path)
        print(new_text)
        self.liststore3[path][2] = new_text
        pass


    def getliststore2(self):
        file = open("liststore2.txt","r+")
        for row in self.liststore2:
            for col in row:
                file.write(col + ',')
            file.write('\n')
        file.close()



    def getliststore3(self):
        file = open("liststore3.txt", "r+")
        for row in self.liststore3:
            for col in row:
                file.write(col + ',')
            file.write('\n')
        file.close()

    def loadliststore2(self):
        file = open("liststore2.txt", "r+")
        lines = file.readlines()

        for line in lines:
            words = line.split(',')
            self.liststore2.append([words[0],words[1]])

    pass

    def loadliststore3(self):
        file = open("liststore3.txt", "r+")
        lines = file.readlines()

        for line in lines:
            words = line.split(',')
            # print(len(words))
            self.liststore3.append([words[0], words[1],words[2]])
        pass

    def on_liststore2_row_changed(self, widget, user_data=None):
        """ Handler for on.row-changed. """
        pass

    def on_treeviewselection2_changed(self,widget, user_data=None):
        """ Handler for on.changed. """
        print("hhhhhhhhhhhhh Nothing Happend hhhhhhhhhhhhh")
        model, row= widget.get_selected()
        if row is not None:
        #print("You selected", model[row][0])
        #print("You selected", model[row][1])
            pass

    def on_window1_destroy(self, object, data=None):
        print ("quit with cancel")
        self.save()

        Gtk.main_quit()

        pass

    def save(self):
        # print("saved")
        # for j in range(2):
        #     for i in range(len(self.liststore2)):
        #         print (self.liststore2[i][j])
                # print(i,j)
                # self.store.insert(i,self.liststore2[i][j])
        self.getliststore2()
        self.getliststore3()

    pass

    def on_button6_clicked(self, widget, user_data=None):
        """ Handler for on.clicked. """
        # self.populate_tree()
        self.loadliststore2()
        self.loadliststore3()
        self.getliststore2()
        self.getliststore3()

        self.messagedialog1.hide()
        pass

def main():
    """ Main entry point for the program. """
    app = App()  # noqa
    return Gtk.main()


if __name__ == '__main__':
    mainret = main()
    sys.exit(mainret)