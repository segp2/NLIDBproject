from pycorenlp import StanfordCoreNLP
from pymysql import connect
from nltk import sent_tokenize, word_tokenize, pos_tag, Tree, WordNetLemmatizer
from nltk.corpus import wordnet as wn


class nlp():
    db = {}
    graph = {}
    relations = []
    cursor = None
    nlp = None

    def __init__(self):
        self.connect_db()
        print("worked db")
        self.find_schema()
        print("schema")
        print(self.db)
        self.connect_NLP()
        print("nlp connection")
        self.find_relations()
        print(self.relations)
        print("found relation")
        self.create_graph()
        print(self.graph)
        print("graph created")
        print(self.find_shortest_path('customer','payment'))
        pass

    # connect to NLP server
    def connect_NLP(self):
        self.nlp = StanfordCoreNLP('http://localhost:9000/')

    # connect to database
    def connect_db(self):
        connection = connect(host='localhost', user='root', passwd='12345', db='dvdrental')
        self.cursor = connection.cursor()


    # extract tables and their attributes from database
    def find_schema(self):
        # db = {}
        self.cursor.execute("show full tables where Table_Type = 'BASE TABLE';")
        result = self.cursor.fetchall()
        for tuple in result:
            self.db[tuple[0]] = []

        for table in self.db:
            self.cursor.execute("show columns from %s;" % table)
            result = self.cursor.fetchall()
            self.db[table] = [tuple[0] for tuple in result]
        # return db

    # extract relations between tables on foreign keys
    def find_relations(self):
        self.cursor.execute("USE INFORMATION_SCHEMA")
        self.cursor.execute(
            "select TABLE_NAME,COLUMN_NAME,REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME from KEY_COLUMN_USAGE WHERE TABLE_SCHEMA='dvdrental' AND CONSTRAINT_NAME!='PRIMARY' AND REFERENCED_TABLE_NAME!='NULL'")
        result = self.cursor.fetchall()
        # fkr = []
        for (refrencing_table, refrencing_att, refrenced_table, refrenced_att) in result:
            self.relations.append(((refrencing_table, refrencing_att), (refrenced_table, refrenced_att)))
        # return fkr
        print(self.relations)
    # translate database to graphs
    def create_graph(self):
        self.cursor.execute("use dvdrental;"); #zaroorat nai ha
        self.graph = {table: [] for table in self.db}
        for ((x, y), (p, q)) in self.relations:
            self.graph[x].append(p)
            self.graph[p].append(x)
        for node in self.graph:
            self.graph[node]=(self.graph[node])
        #print(self.graph)
        # return self.graph

    # find the shortest paths from one node to another
    def find_shortest_path(self,start, end, path=[]):
        path = path + [start]
        if start == end:
            return path
        if not start in self.graph:
            return None
        shortest = None
        for node in self.graph[start]:
            if node not in path:
                newpath = self.find_shortest_path(node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest

    # find the paths from one node to another
    def find_path(self,start_table, end_table, visited_nodes=[]):
        if start_table == end_table:
            return [end_table]
        # print(self.graph)
        print(start_table)
        if len(self.graph[start_table]) == 0:
            return []
        visited_neigbours = []
        for node in self.graph[start_table]:
            if node not in visited_nodes:
                visited_neigbours.append(node)
                path = [start_table] + self.find_path(node, end_table, visited_nodes + visited_neigbours)
                if path[len(path) - 1] == end_table:
                    return path
        return []

    # parse the input in a tree
    def parse_input(self, inp):
        # output = []
        output = self.nlp.annotate(inp, properties={
            'annotators': 'parse',
            'outputFormat': 'json'
        })
        # print(output)
        return output

    # extract tree from the parsed tree
    def get_tree(self, output):
        parsed_output = output['sentences'][0]['parse']
        tree = Tree.fromstring(parsed_output)
        return tree

    # if dependency of a parsed tree is 'nsubj'
    def get_nsubj(self, output):
        dep_array = output['sentences'][0]['basicDependencies']
        # print(dep_array)
        result = []
        for d in dep_array:
            if d['dep'] == 'nsubj':
                result.append((d['governorGloss'], d['dependentGloss']))
        # print(result)
        return result

    # extract clauses and noun phrases from tree
    def clausamite(self, c_tree):
        subtexts = []
        for subtree in c_tree.subtrees():
            if subtree.label() == "S" or subtree.label() == "SBAR":
                subtexts.append(' '.join(subtree.leaves()))

        clauses = subtexts[:]
        for i in reversed(range(len(subtexts) - 1)):
            if subtexts[i].find(subtexts[i + 1]) != -1:
                clauses[i] = subtexts[i][0:subtexts[i].index(subtexts[i + 1])]

        clauses = [clause.strip() for clause in clauses if len(clause) > 0]
        return clauses

    # extract nouns from clauses
    def find_nouns(self, inp):
        nouns = []
        pos_tagged = pos_tag(word_tokenize(inp))
        is_noun = lambda pos: pos[:2] == 'NN'
        for (word, pos) in pos_tag(word_tokenize(inp)):
            if is_noun(pos):
                nouns.append(word)
        return nouns

    # extract tables from clauses
    def find_tables(self, nouns):
        output = []
        for noun in nouns:
            if noun in self.db:
                output.append(noun)
        return output

    # extract respective table's attributes
    def find_table_attributes(self, inp, tables):
        output = []
        for noun in inp:
            for table in self.db:
                for attr in self.db[table]:
                    if noun in attr.replace('_', ' '):
                        if table in tables and noun != table:
                            output.append((table, attr))
        return output


    # calculate and return join statement
    def join_query(self, path):
        if len(path) < 2:
            return ''
        query = ''
        for index in range(1, len(path)):
            for ((refrencing_table, refrencing_attribute), (referenced_table, referenced_attribute)) in self.relations:
                if refrencing_table == path[index - 1] and referenced_table == path[index]:
                    query = query + 'join ' + path[
                        index] + ' on(' + refrencing_table + '.' + refrencing_attribute + '=' + referenced_table + '.' + referenced_attribute + ') '
                    break
                elif refrencing_table == path[index] and referenced_table == path[index - 1]:
                    query = query + 'join ' + path[
                        index] + ' on (' + referenced_table + '.' + referenced_attribute + '=' + refrencing_table + '.' + refrencing_attribute + ') '
                    break
        return query


    # generate output query
    def generate_query(self, clauses,tables, table_attributes,path):
        where_list = ['where', 'which', 'who', 'whom', 'whose']

        sqlQuery = "SELECT "
        if len(table_attributes) == 0:
            if self.count_aggregate(clauses):
                sqlQuery += "count(*) "
            else:
                sqlQuery += "* "
        else:
            aggregate = ''
            if self.rest_aggregate(clauses) is not '':
                aggregate = self.rest_aggregate(clauses)

            attributes = []
            for (table, attribute) in table_attributes:
                attributes.append(attribute)

            for attribute in attributes:
                if attribute in aggregate:
                    attributes.remove(attribute)

            sqlQuery += aggregate
            sqlQuery += " " + ','.join(attributes) + " "

        if len(tables) > 0:
            sqlQuery += "FROM " + tables[0] + " "

        # where_clause = ''
        # # tok = []
        # for i in range(0, len(clauses)):
        #     if clauses[i] in where_list:
        #         where_clause = clauses[i + 1]
        #         print(where_clause)
        #
        # tok = word_tokenize(where_clause)
        # n = self.find_nouns(self, where_clause)
        # n = self.lemmatizing(self, n)
        # tab_attributes = []
        # for tab in tables:
        #     tab_attributes += self.find_table_attributes(self, n, tab, db)
        #
        # if "is" in tok and not ("between" in tok) and len(conditionColumns) == 1:
        #     val = tok[tok.index("is") + 1]
        #     sqlQuery += conditionColumns[0] + " = '" + val + "'"
        # if "between" in tok and len(conditionColumns) == 1:
        #     sqlQuery += conditionColumns[0] + " between " + tok[tok.index("between") + 1] + " and " + tok[
        #         tok.index("between") + 3]

        # for clause in clauses:
        #     if clause in where_list:
        #
        ans = ''
        sqlQuery += self.join_query(path)
        where_clause = clauses[2]
        tok = word_tokenize(where_clause)
        n = self.find_nouns(clauses[2])
        n = self.lemmatizing(n)
        print(n)
        tab = self.find_tables(n)
        tab_att = [()]
        if len(tab) > 0:
            tab_att = self.find_table_attributes(n, tab)

        # elif len(table_attributes) > 0:
        #     for tble in tables:
        #         for att in self.db[tble]:
        #             if att in table_attributes:
        #                 ans = att
        (ans,ans2) = tab_att[0]
        ans3 = ans+"."+ans2
        sqlQuery += " WHERE "
        if "is" in tok:
            sqlQuery += ans3 + " = "
            val = tok[tok.index("is") + 1]
        if "less" in tok:
            val = tok[tok.index("less") + 2]
        sqlQuery += val
        return sqlQuery

    # stip pural nouns to singular
    def lemmatizing(self, nouns):
        output = []
        lemmatizer = WordNetLemmatizer()
        for noun in nouns:
            lemma = lemmatizer.lemmatize(noun)
            output.append(lemma)
            #     print(wn.synset(lemma).lemma_nagmes())
        return output

    # pre-process the input
    def pre_processing(self,inp):
        revised = word_tokenize(inp)
        for word in revised:
            if '.' not in revised:
                revised.append('.')

        first_parse = self.parse_input(inp)
        nsubjs = self.get_nsubj(first_parse)
        # print(nsubjs)

        for (first, second) in nsubjs:
            inp = inp.replace(first, '\'' + first + '\'', 1)
        if inp[-1] != '.':
            inp = inp + '.'

        return ' '.join(revised)

    # look if query requires aggregate function 'count(*)'
    def count_aggregate(self, clauses):
        count = ['total number', 'number', 'count', 'many']
        for count_aggregate in count:
            if count_aggregate in clauses[0]:
                return True
        return False

    # look if query requires other aggregate functions like 'sum','min','max'
    def rest_aggregate(self, clauses):
        sum = ['total', 'sum']
        min = ['least', 'minimum', 'smallest', 'shortest']
        max = ['largest', 'maximum', 'longest']

        output = self.parse_input(clauses[0])
        dep_array = output['sentences'][0]['basicDependencies']
        result = []
        for d in dep_array:
            if d['dep'] == 'nmod' or d['dep'] == 'amod':
                result.append((d['governorGloss'], d['dependentGloss']))
        # print(result)

        if len(result) > 0:
            if result[0][0] in sum:
                return 'sum(' + result[0][1] + ')'
            elif result[0][0] in min:
                return 'min(' + result[0][1] + ')'
            elif result[0][0] in max:
                return 'max(' + result[0][1] + ')'

            if result[0][1] in sum:
                return 'sum(' + result[0][0] + ')'
            elif result[0][1] in min:
                return 'min(' + result[0][0] + ')'
            elif result[0][1] in max:
                return 'max(' + result[0][0] + ')'
        return ''


    # main function
    def main(self, inps, progressbar1):
        # cursor = self.connect_db(self)
        # nlp = self.connect_NLP()
        # db = self.find_schema(self, cursor)
        # relations = self.find_relations(self, cursor)
        # graph = self.create_graph(self, relations, db, cursor)

        # inps = input('ENTER QUERY:\n')


        # progressbar1.set_fraction(0.1)
        progressbar1.set_fraction(1)
        print("1")
        inp = self.pre_processing(inps)
        print("2")
        # print(inp)
        output = self.parse_input(inp)
        print("3")

        tree = self.get_tree(output)
        print("4")
        clauses = self.clausamite(tree)
        # print(clauses)
        print("5")
        nouns = []
        for clause in clauses:
            print(clause)
            nouns += self.find_nouns(clause)
        nouns = self.lemmatizing(nouns)
        print("6")
        tables = self.find_tables(nouns)
        print("7")
        table_attributes = self.find_table_attributes(nouns, tables)
        print("8")
        path = []
        print("9")
        print("the shortest path")
        print(self.find_shortest_path('customer', 'payment'))
        if len(tables) > 1:
            print("the tables are:")
            print(tables)
            path = self.find_shortest_path(tables[0], tables[1])
            print("the path")
            print(path)
        print("10")
        sqlQuery = self.generate_query(clauses,tables, table_attributes,path)
        print("11")
        # sqlQuery = ""
        return (sqlQuery)
        # return (sqlQuery)


if __name__ == '__main__':
    inp = input("Enter Query")
    #main(nlp)